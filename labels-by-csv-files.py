import glob
import sys
import os
import re

if len(sys.argv) < 4:
    print("<label> <directory> <L|U|B>")
    sys.exit(1)

label = sys.argv[1]
start_dir = sys.argv[2]
image_type = sys.argv[3]
no_label = "No" + sys.argv[1]

if image_type == "L":
    image_type = "_L.png"
elif image_type == "U":
    image_type = "_U.png"
else:
    image_type  = None

csv_files = glob.glob(start_dir + '/**/*.csv', recursive=True)

pattern = re.compile(r"^(.+);\d{8,20};.*;(\w+)$")

print("file sum no_sum sum_total no_sum_total")

sum_total = 0
no_sum_total = 0
for csv_file in csv_files:
    sum = 0
    no_sum = 0
    with open(csv_file, "r") as fp:
        for line in fp:
            m = pattern.match(line)

            if not m:
                continue

            line_img_file = m.group(1)
            line_label = m.group(2)

            if line_label == label:
                if image_type == None:
                    sum = sum + 1
                elif line_img_file.endswith(image_type):
                    sum = sum + 1

            if line_label == no_label:
                if image_type == None:
                    no_sum = no_sum + 1
                elif line_img_file.endswith(image_type):
                    no_sum = no_sum + 1

    sum_total += sum
    no_sum_total += no_sum

    if sum > 5 or no_sum > 5:
        print(csv_file, sum, no_sum, sum_total, no_sum_total)