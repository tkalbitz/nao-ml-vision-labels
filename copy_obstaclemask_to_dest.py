#!/usr/bin/python3

import multiprocessing
import os
import shlex
import shutil
import stat
import subprocess
import sys
import time

def find_files_by_extension(root_dir, extension):

    file_list = []

    for root, dirs, files in os.walk(root_dir):
        for file in files:
            if file.endswith(extension):
                file_list.append(os.path.join(root, file))
    return file_list

def find_files_by_extension_as_map(root_dir, extension):

    file_dict = {}

    for root, dirs, files in os.walk(root_dir):
        for file in files:
            if file.endswith(extension):
                file_dict[file] = root
    return file_dict

if len(sys.argv) < 3:
    print("<image dir> <obstacle dir>")

png_file_dict = find_files_by_extension_as_map(sys.argv[1], "png")
obstacle_file_list = find_files_by_extension(sys.argv[2], "obstaclemask")

for oFilePath in obstacle_file_list:
    oFileName = os.path.basename(oFilePath)
    pngFileName, ignore = os.path.splitext(oFileName)

    if pngFileName in png_file_dict:
        if os.path.exists(os.path.join(png_file_dict[pngFileName], oFileName)):
            print("Exists {} in {}".format(oFileName, png_file_dict[pngFileName]))
        else:
            print("Copy {} to {}".format(oFilePath, png_file_dict[pngFileName]))
            shutil.copy2(oFilePath, png_file_dict[pngFileName])
    else:
        print("No dir found {}".format(oFilePath))