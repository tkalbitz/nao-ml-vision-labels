import glob
import math
import sys
import os
import re

if len(sys.argv) < 4:
    print("<label> <directory> <L|U|B>")
    sys.exit(1)

label = sys.argv[1]
start_dir = sys.argv[2]
image_type = sys.argv[3]
no_label = "No" + sys.argv[1]

if image_type == "L":
    image_type = "_L.png"
elif image_type == "U":
    image_type = "_U.png"
else:
    image_type  = None

csv_files = glob.glob(start_dir + '/**/*.csv', recursive=True)

pattern = re.compile(r"^(.+);\d{8,20};.*;(\d+);(\d+);(\d+);(\d+);(\w+)$")

print("diameter count")

distribution = {}
for csv_file in csv_files:
    with open(csv_file, "r") as fp:
        for line in fp:
            m = pattern.match(line)

            if not m:
                continue

            line_img_file = m.group(1)
            x1 = int(m.group(2))
            y1 = int(m.group(3))
            x2 = int(m.group(4))
            y2 = int(m.group(5))
            line_label = m.group(6)

            if line_label == label:
                if not (image_type == None or line_img_file.endswith(image_type)):
                    continue

                diam = math.ceil(math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)))

                # if diam > 160:
                #     print(line_img_file)
                    # print(line, end="")

                if diam in distribution:
                    distribution[diam] += 1
                else:
                    distribution[diam] = 1

max_key = max(distribution.keys()) + 1

for i in range(max_key):
    if i in distribution:
        print(i, distribution[i])
    else:
        print(i, 0)