import pathlib
import sys
import os
import shutil

if len(sys.argv) < 3:
    print("<in dir> <out dir>")
    exit(0)

in_dir = sys.argv[1]
out_dir = sys.argv[2]

if not os.path.exists(in_dir):
    print("In dir does not exists!")
    exit(1)

if not os.path.exists(out_dir):
    print("Out dir does not exists!")
    exit(1)

ofiles = []
for p, d, f in os.walk(in_dir):
    for file in f:
        if file.endswith('.obstaclemask'):
            ofiles.append(os.path.join(p, file[:-len('.obstaclemask')]))
            ofiles.append(os.path.join(p, file))

out_dir_p = pathlib.Path(out_dir)
for f in ofiles:
    p = out_dir_p / f
    p.parent.mkdir(parents=True, exist_ok=True)
    print(f, str(p))
    shutil.copy2(f, str(p))